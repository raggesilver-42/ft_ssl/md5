/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_priv.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/30 14:16:45 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/07/05 15:29:55 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_priv.h"

const void	*g_ssl_commands[] = {
	"md5", ft_ssl_md5,
	"sha256", ft_ssl_sha256,
	NULL, NULL
};

uint32_t		rol_left(uint32_t v, uint32_t amt)
{
	uint32_t	msk1;

	msk1 = (1 << amt) - 1;
	return ((v >> (32 - amt)) & msk1) | ((v << amt) & ~msk1);
}
