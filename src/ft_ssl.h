/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/29 00:51:20 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/07/22 16:01:28 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_H
# define FT_SSL_H

# include "libft.h"

/*
** The ssl functions declared in this file are API functions that can be used
** in other programs. The dispatch function uses private functions that should
** not be called directly from anywhere else
*/

t_string		*ft_md5(t_string *src, uint flags);
t_string		*ft_sha256(t_string *src);

char			*fast_uint8_to_hex_str(uint8_t *p, size_t len);

#endif
