/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/29 00:44:38 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/07/22 16:41:44 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf/ft_printf.h"
#include "ft_ssl.h"
#include "ft_ssl_priv.h"

const char	*g_ft_ssl_help =
"usage: ft_ssl command [command opts] [command args]";

/*
** TODO: find some better place for this function
** FIXME: add docstrings for this function
*/

char			*fast_uint8_to_hex_str(uint8_t *p, size_t len)
{
	static char	*chars = "0123456789abcdef";
	char		*res;
	size_t		i;
	uint8_t		a;

	res = malloc((len * 2) + 1);
	i = 0;
	while (i < len)
	{
		a = ((p[i] >> 4) & 0x0F);
		res[i * 2] = chars[a];
		a = (p[i] & 0x0F);
		res[(i * 2) + 1] = chars[a];
		i++;
	}
	res[i * 2] = 0;
	return (res);
}

void			*get_command(const char *cmd)
{
	const void	**cmds;

	cmds = g_ssl_commands;
	while (*cmds)
	{
		if (ft_strcmp(*cmds, cmd) == 0)
			return ((void *)*(++cmds));
		cmds += 2;
	}
	return (NULL);
}

int				main(int argc, const char **argv)
{
	void		*cmd;

	if (argc < 2 || ft_strcmp(argv[1], "--help") == 0)
	{
		ft_putendl_fd(g_ft_ssl_help, (argc < 2) ? 2 : 1);
		RETURN_VAL_IF_FAIL(1, (argc > 1));
	}
	if ((cmd = get_command(*(++argv))))
		FT_SSL_CMD(cmd)(++argv);
	else
	{
		ft_printf("invalid command '%s'\n", *(argv));
		ft_putendl_fd(g_ft_ssl_help, (argc < 2) ? 2 : 1);
	}
	return (0);
}
