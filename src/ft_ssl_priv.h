/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_priv.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/29 01:01:12 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/07/05 15:30:33 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_PRIV_H
# define FT_SSL_PRIV_H

# include "ft_ssl.h"
# include "libft.h"

/*
** Naming for t_ssl_dig members:
**	h = hash
**	l = letters (a, b, c, and d for md5 for example)
**	c = counters
**	w = word pointer
**	res = result pointer (array of uint32_t)
*/

typedef struct		s_ssl_dig
{
	uint32_t		h[10];
	uint32_t		l[10];
	size_t			ct[5];
	uint32_t		*w;
	uint32_t		tmp[5];
	uint32_t		*res;
	uint			flags;
	t_file			*file;
}					t_ssl_dig;

/*
** These functions are wrapper functions that call the real ones from cmd args
** and print the result to stdout
*/

void				ft_ssl_md5(const char **args);
void				ft_ssl_sha256(const char **args);

#define FT_SSL_CMD(x) (((void(*)(const char **))x))

extern const void	*g_ssl_commands[];

/*
** Aux functions
*/

uint32_t		rol_left(uint32_t v, uint32_t amt);

#endif
