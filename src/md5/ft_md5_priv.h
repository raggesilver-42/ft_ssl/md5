/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_md5_priv.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/02 17:24:18 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/07/05 23:46:43 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MD5_PRIV_H
# define FT_MD5_PRIV_H

# include "../ft_ssl.h"
# include <stdlib.h>

typedef uint32_t		(*t_md5_dig_func)(uint32_t b, uint32_t c, uint32_t d);

extern t_md5_dig_func	g_md5_dig_funcs[4];

enum					e_md5_dig_vars
{
	MD5_L_A = 0,
	MD5_L_B = 1,
	MD5_L_C = 2,
	MD5_L_D = 3,
	MD5_CT_I = 0,
	MD5_CT_J = 1,
	MD5_TMP_F = 0,
	MD5_TMP_G = 1,
	MD5_TMP_TMP = 2
};

enum					e_md5_flags
{
	MD5_FLAG_P = 1,
	MD5_FLAG_Q = 2,
	MD5_FLAG_S = 4,
	MD5_FLAG_R = 8
};

#endif
