/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_md5.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/30 14:53:49 by pqueiroz          #+#    #+#             */
<<<<<<< HEAD
/*   Updated: 2019/07/22 14:08:08 by pqueiroz         ###   ########.fr       */
=======
/*   Updated: 2019/07/11 23:04:49 by pqueiroz         ###   ########.fr       */
>>>>>>> d7bd8cb7c5063958d7974898550d6d4038c6b6de
/*                                                                            */
/* ************************************************************************** */

#include "../ft_ssl.h"
#include "../ft_ssl_priv.h"
#include "ft_md5_priv.h"

#include "ft_printf/ft_printf.h"

/*
** MD5 static constants ========================================================
**
** Constant sines
*/

static uint32_t	g_k[] = {
	0xD76AA478, 0xE8C7B756, 0x242070DB, 0xC1BDCEEE,
	0xF57C0FAF, 0x4787C62A, 0xA8304613, 0xFD469501,
	0x698098D8, 0x8B44F7AF, 0xFFFF5BB1, 0x895CD7BE,
	0x6B901122, 0xFD987193, 0xA679438E, 0x49B40821,
	0xF61E2562, 0xC040B340, 0x265E5A51, 0xE9B6C7AA,
	0xD62F105D, 0x02441453, 0xD8A1E681, 0xE7D3FBC8,
	0x21E1CDE6, 0xC33707D6, 0xF4D50D87, 0x455A14ED,
	0xA9E3E905, 0xFCEFA3F8, 0x676F02D9, 0x8D2A4C8A,
	0xFFFA3942, 0x8771F681, 0x6D9D6122, 0xFDE5380C,
	0xA4BEEA44, 0x4BDECFA9, 0xF6BB4B60, 0xBEBFBC70,
	0x289B7EC6, 0xEAA127FA, 0xD4EF3085, 0x04881D05,
	0xD9D4D039, 0xE6DB99E5, 0x1FA27CF8, 0xC4AC5665,
	0xF4292244, 0x432AFF97, 0xAB9423A7, 0xFC93A039,
	0x655B59C3, 0x8F0CCC92, 0xFFEFF47D, 0x85845DD1,
	0x6FA87E4F, 0xFE2CE6E0, 0xA3014314, 0x4E0811A1,
	0xF7537E82, 0xBD3AF235, 0x2AD7D2BB, 0xEB86D391
};

/*
** Shift amounts
*/

static uint32_t	g_r[] = {
	7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
	5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20,
	4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
	6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21
};

static uint32_t	g_h[] = { 0x67452301, 0xEFCDAB89, 0x98BADCFE, 0x10325476 };
static short	g_m[] = { 1, 5, 3, 7 };
static short	g_o[] = { 0, 1, 5, 0 };

/*
** Actual functions ============================================================
*/

/*
** Pad to the final length. already includes the space for the final 8 bytes
** used to store the length
*/

static void		fk_md5_step1(t_string *src)
{
	t_string	*padding;
	char		*tmp;
	size_t		len;

	len = ((1 + (src->length + 8) / 64) * 64) - src->length;
	tmp = ft_strnew(len);
	padding = ft_string_new_s(&tmp);
	padding->data[0] = 0x80;
	padding->length = len;
	ft_string_real_append(src, padding);
	ft_string_destroy(&padding);
}

/*
** Ovewrite the last 8 bytes with the original string length
*/

static void		fk_md5_step2(t_string *src, size_t b)
{
	uint32_t	sz;

	sz = b * 8;
	ft_memcpy(src->data + (src->length - 8), &sz, 4);
}

/*
** Process a 64-byte chunk
*/

static void		fk_md5_step3_1(t_ssl_dig *dig)
{
	dig->ct[MD5_CT_J] = 0;
	while (dig->ct[MD5_CT_J] < 64)
	{
		dig->tmp[MD5_TMP_F] = g_md5_dig_funcs[dig->ct[MD5_CT_J] / 16](
			dig->l[1], dig->l[2], dig->l[3]);
		dig->tmp[MD5_TMP_G] = ((g_m[dig->ct[MD5_CT_J] / 16] *
			dig->ct[MD5_CT_J]) + g_o[dig->ct[MD5_CT_J] / 16]) % 16;
		dig->tmp[MD5_TMP_TMP] = dig->l[3];
		dig->l[3] = dig->l[2];
		dig->l[2] = dig->l[1];
		dig->l[1] = dig->l[1] + rol_left(((dig->l[0]) + dig->tmp[MD5_TMP_F] +
			g_k[dig->ct[MD5_CT_J]] + dig->w[dig->tmp[MD5_TMP_G]]),
			g_r[dig->ct[MD5_CT_J]]);
		dig->l[0] = dig->tmp[MD5_TMP_TMP];
		dig->ct[MD5_CT_J]++;
	}
}

/*
** Process all chunks in the message
*/

static void		fk_md5_step3(t_string *src, t_ssl_dig *dig)
{
	ft_memcpy(dig->h, g_h, sizeof(g_h[0]) * 4);
	dig->ct[MD5_CT_I] = 0;
	while (dig->ct[MD5_CT_I] < src->length)
	{
		ft_memcpy(dig->l, dig->h, sizeof(dig->h[0]) * 4);
		dig->w = (uint32_t *)(src->data + dig->ct[MD5_CT_I]);
		fk_md5_step3_1(dig);
		dig->h[0] += dig->l[0];
		dig->h[1] += dig->l[1];
		dig->h[2] += dig->l[2];
		dig->h[3] += dig->l[3];
		dig->ct[MD5_CT_I] += 64;
	}
	dig->res = malloc(sizeof(*dig->res) * 4);
	ft_memcpy(dig->res, dig->h, sizeof(*dig->res) * 4);
}

t_string		*ft_md5(t_string *src, uint flags)
{
	size_t		orig_len;
	t_ssl_dig	*dig;
	char		*tmp;

	(void)flags;
	orig_len = src->length;
	dig = malloc(sizeof(*dig));
	fk_md5_step1(src);
	fk_md5_step2(src, orig_len);
	fk_md5_step3(src, dig);
	tmp = fast_uint8_to_hex_str((uint8_t *)dig->res, 16);
	ft_memdel((void **)&dig->res);
	ft_memdel((void **)&dig);
	return (ft_string_new_s(&tmp));
}
