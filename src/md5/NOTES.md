# MD5

## The algorithm

### Step 1 - Padding
```C
// src/md5/ft_md5.c
ft_md5(t_string *src, t_ssl_flags flags)
```

The string **will always be padded** to a length multiple of 56.

Padding works by adding the first byte `1000 0000` followed by n 0 bytes.
