/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_md5.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/30 14:23:24 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/07/15 18:35:23 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_ssl.h"
#include "../ft_ssl_priv.h"
#include "libft.h"

#include "ft_printf/ft_printf.h"
#include "argparser/argparser.h"

static t_arg		*g_args[] = {
	&(t_arg){ .str="-r", .val=(1 << 0), .on_error=NULL },
	&(t_arg){ .str="-s", .val=(1 << 1), .on_error=NULL },
	&(t_arg){ .str="-p", .val=(1 << 2), .on_error=NULL },
	&(t_arg){ .str="-q", .val=(1 << 3), .on_error=NULL },
	NULL
};

static t_argparser	*g_ap = &(t_argparser){
	.target=NULL,
	.flags=0,
	.args=g_args,
	.on_extra=NULL
};

static void			fk_on_extra(t_argparser *self)
{
	ft_printf("Wrong arg '%s'\n", *self->argv);
}

static t_string		*fk_get_from_file(const char *path)
{
	t_string		*res;
	t_file			file;

	res = NULL;
	file = ft_fopen(path, O_RDONLY);
	if (file.fd != -1)
		res = ft_fread(file);
	ft_fclose(file);
	return (res);
}

/*
** This function will process args, and maybe call ft_md5 with the proper
** arguments and flags
*/

void				ft_ssl_md5(const char **args)
{
	t_string		*res;
	t_string		*s;

	g_ap->argv = args;
	g_ap->on_extra = &fk_on_extra;
	ft_parse_args(g_ap);
	if (g_ap->flags == 0 && *g_ap->target)
	{
		while (*g_ap->target)
		{
			s = fk_get_from_file(*g_ap->target);
			if (!s)
			{
				ft_printf("%s: No such file or directory\n", *g_ap->target++);
				continue ;
			}
			res = ft_md5(s, g_ap->flags);
			ft_printf("MD5(%s)= %s\n", *g_ap->target, res->data);
			ft_string_destroy(&res);
			ft_string_destroy(&s);
			g_ap->target++;
		}
	}
	else if ((g_ap->flags & (1 << 2)) || !*g_ap->target)
	{
		s = ft_string_new("");
		char *tmp = NULL;
		while (ft_readln(0, &tmp) > 0)
		{
			ft_string_append(s, tmp);
			ft_string_append(s, "\n");
			ft_strdel(&tmp);
		}
		(g_ap->flags & (1 << 2)) ? ft_printf("%s", s->data) : 0;
		res = ft_md5(s, g_ap->flags);
		ft_string_destroy(&s);
		ft_printf("%s\n", res->data);
		ft_string_destroy(&res);
	}
	else if (g_ap->flags & (1 << 1))
	{
		while (*g_ap->target)
		{
			s = ft_string_new(*g_ap->target);
			res = ft_md5(s, g_ap->flags);
			ft_printf("MD5(\"%s\")= %s\n", *g_ap->target, res->data);
			ft_string_destroy(&res);
			ft_string_destroy(&s);
			g_ap->target++;
		}
	}
}
