/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_md5_priv.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/02 17:25:09 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/07/02 20:26:16 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_md5_priv.h"
#include <stdlib.h>

/*
** The helper functions work like this:
**
** 	turn 4 chars into one uint32_t (pseudo C code, might have do do void *)
**
**	char s[16];
**	uint32_t abcd[4] = &c;
**
** each number in abcd is actually 4 bytes (chars) from s.
**
** Aux functions ===============================================================
*/

uint32_t		fk_ssl_md5_fn_f0(uint32_t b, uint32_t c, uint32_t d)
{
	return ((b & c) | ((~b) & d));
}

uint32_t		fk_ssl_md5_fn_f1(uint32_t b, uint32_t c, uint32_t d)
{
	return ((d & b) | ((~d) & c));
}

uint32_t		fk_ssl_md5_fn_f2(uint32_t b, uint32_t c, uint32_t d)
{
	return (b ^ c ^ d);
}

uint32_t		fk_ssl_md5_fn_f3(uint32_t b, uint32_t c, uint32_t d)
{
	return (c ^ (b | (~d)));
}

t_md5_dig_func	g_md5_dig_funcs[4] = {
	fk_ssl_md5_fn_f0,
	fk_ssl_md5_fn_f1,
	fk_ssl_md5_fn_f2,
	fk_ssl_md5_fn_f3
};
