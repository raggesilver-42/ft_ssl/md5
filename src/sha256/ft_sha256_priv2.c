/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sha256_priv2.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 11:11:47 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/07/22 14:06:04 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sha256_priv.h"
#include <stdlib.h>

uint32_t	fk_ssl_sha256_fn_f5(uint32_t x, uint32_t y, uint32_t z)
{
	(void)y;
	(void)z;
	return (ROTR(17, x) ^ ROTR(19, x) ^ SHR(10, x));
}
