/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_sha256.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/30 14:23:24 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/07/22 16:04:21 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_ssl.h"
#include "../ft_ssl_priv.h"
#include "ft_sha256.h"

/*
** This function will process args, and maybe call ft_sha256 with the proper
** arguments and flags
*/

void	ft_ssl_sha256(const char **args)
{
	t_string	*src;
	t_string	*res;
	t_file		f;

	if (*args)
	{
		f = ft_fopen(*args, O_RDONLY);
		src = ft_fread(f);
		res = ft_sha256(src);
		ft_fclose(f);
		ft_string_destroy(&src);
		ft_string_destroy(&res);
	}
}
