/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sha256_priv.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 11:11:47 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/07/22 14:05:46 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SHA256_PRIV_H
# define FT_SHA256_PRIV_H

# include "../ft_ssl.h"
# include "../ft_ssl_priv.h"

# define WLEN 32

# define ROTL(n, x) (((x) << (n)) | ((x) >> (WLEN - (n))))
# define ROTR(n, x) (((x) >> (n)) | ((x) << (WLEN - (n))))

# define SHR(n, x) ((x) >> (n))

typedef uint32_t			(*t_sha256_dig_func)(
								uint32_t b, uint32_t c, uint32_t d);

extern t_sha256_dig_func	g_sha256dig_funcs[6];

uint32_t					fk_ssl_sha256_fn_f0(
									uint32_t x, uint32_t y, uint32_t z);
uint32_t					fk_ssl_sha256_fn_f1(
									uint32_t x, uint32_t y, uint32_t z);
uint32_t					fk_ssl_sha256_fn_f2(
									uint32_t x, uint32_t y, uint32_t z);
uint32_t					fk_ssl_sha256_fn_f3(
									uint32_t x, uint32_t y, uint32_t z);
uint32_t					fk_ssl_sha256_fn_f4(
									uint32_t x, uint32_t y, uint32_t z);
uint32_t					fk_ssl_sha256_fn_f5(
									uint32_t x, uint32_t y, uint32_t z);

#endif
