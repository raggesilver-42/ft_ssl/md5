/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sha256.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/30 14:23:24 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/07/22 16:12:01 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_ssl.h"
#include "../ft_ssl_priv.h"
#include "ft_sha256_priv.h"

#include "ft_printf/ft_printf.h"

/*
** SHA256 static constants =====================================================
**
** Constants
*/

// static uint32_t	g_k[] = {
// 	0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1,
// 	0x923F82A4, 0xAB1C5ED5, 0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3,
// 	0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174, 0xE49B69C1, 0xEFBE4786,
// 	0x0FC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA,
// 	0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147,
// 	0x06CA6351, 0x14292967, 0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13,
// 	0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85, 0xA2BFE8A1, 0xA81A664B,
// 	0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070,
// 	0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A,
// 	0x5B9CCA4F, 0x682E6FF3, 0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208,
// 	0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2
// };

// static uint32_t	g_h[] = {
// 	0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C,
// 	0x1F83D9AB, 0x5BE0CD19
// };

static void		fk_sha256_step1(t_string *buf)
{
	t_string	*padding;
	char		*tmp;
	size_t		len;

	len = ((1 + (buf->length + 8) / 64) * 64) - buf->length;
	tmp = ft_strnew(len);
	padding = ft_string_new_s(&tmp);
	padding->data[0] = 0x80;
	padding->length = len;
	ft_string_real_append(buf, padding);
	ft_string_destroy(&padding);
}

t_string		*ft_sha256(t_string *src)
{
	t_string	*buf;
	char		*tmp;

	buf = ft_string_clone(src);
	fk_sha256_step1(buf);
	ft_printf("Buf len: %lu\n", buf->length);
	tmp = ft_strnew(64);
	ft_string_destroy(&buf);
	return (ft_string_new_s(&tmp));
}
