/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sha256_priv.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 11:11:47 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/07/22 14:05:54 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sha256_priv.h"
#include <stdlib.h>

uint32_t	fk_ssl_sha256_fn_f0(uint32_t x, uint32_t y, uint32_t z)
{
	return ((x & y) ^ ((~x) & z));
}

uint32_t	fk_ssl_sha256_fn_f1(uint32_t x, uint32_t y, uint32_t z)
{
	return ((x & y) ^ (x & z) ^ (y & z));
}

uint32_t	fk_ssl_sha256_fn_f2(uint32_t x, uint32_t y, uint32_t z)
{
	(void)y;
	(void)z;
	return (ROTR(2, x) ^ ROTR(13, x) ^ ROTR(22, x));
}

uint32_t	fk_ssl_sha256_fn_f3(uint32_t x, uint32_t y, uint32_t z)
{
	(void)y;
	(void)z;
	return (ROTR(6, x) ^ ROTR(11, x) ^ ROTR(25, x));
}

uint32_t	fk_ssl_sha256_fn_f4(uint32_t x, uint32_t y, uint32_t z)
{
	(void)y;
	(void)z;
	return (ROTR(7, x) ^ ROTR(18, x) ^ SHR(3, x));
}

t_sha256_dig_func	g_sha256dig_funcs[6] = {
	fk_ssl_sha256_fn_f0,
	fk_ssl_sha256_fn_f1,
	fk_ssl_sha256_fn_f2,
	fk_ssl_sha256_fn_f3,
	fk_ssl_sha256_fn_f4,
	fk_ssl_sha256_fn_f5
};
