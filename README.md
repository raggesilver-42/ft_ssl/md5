# ft_ssl_md5

My implementation of `openssl` - first project of the ft_ssl branch. For this first project we need to implement `MD5` and `SHA256`.

Subject: [here](https://cdn.intra.42.fr/pdf/pdf/1380/ft_ssl_md5.en.pdf)
